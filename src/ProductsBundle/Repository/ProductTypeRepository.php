<?php

namespace ProductsBundle\Repository;


use Doctrine\ORM\EntityRepository;
use ProductsBundle\Entity\ProductType;

class ProductTypeRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function getListProductsTypeWithProductsArray()
    {
        return $this
            ->createQueryBuilder('type')
            ->select('type', 'products')
            ->leftJoin('type.products', 'products')
            ->orderBy('type.modifiedAt', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @return array
     */
    public function getListProductsTypeArray()
    {
        return $this
            ->createQueryBuilder('type')
            ->select('type')
            ->orderBy('type.modifiedAt', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }

    public function getAllTypesQueryBuilder()
    {
        return $this
            ->createQueryBuilder('type')
            ->select('type')
            ->orderBy('type.modifiedAt', 'DESC');
    }
}
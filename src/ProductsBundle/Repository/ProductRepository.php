<?php

namespace ProductsBundle\Repository;


use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    public function getListProductsArray()
    {
        return $this->createQueryBuilder('product')
            ->select('product')
            ->orderBy('product.modifiedAt', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }
}
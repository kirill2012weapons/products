<?php

namespace ProductsBundle\Service;


use Doctrine\ORM\EntityManagerInterface;
use ProductsBundle\Entity\Product;
use ProductsBundle\Entity\ProductType;

class Price
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getSumPriceForAllProductTypes()
    {
        $res = $this->getManager()
            ->createQueryBuilder()
            ->select('type.name as type_name, sum(products.price) as count_price')
            ->from(ProductType::class, 'type')
            ->leftJoin('type.products', 'products')
            ->groupBy('type.name')
            ->orderBy('count_price', 'DESC')
            ->getQuery()
            ->getArrayResult();

        foreach ($res as $key => &$value) {
            $value['count_price'] = is_null($value['count_price']) ? '0.00' : $value['count_price'];
        }

        return $res;
    }

    public function getManager()
    {
        return $this->em;
    }
}
<?php

namespace ProductsBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PriceController extends Controller
{

    public function indexAction()
    {

        $result = $this->container->get('price_manager');

        return $this->render('@products/Price/index.html.twig', [
            'result' => $result->getSumPriceForAllProductTypes(),
        ]);
    }

}

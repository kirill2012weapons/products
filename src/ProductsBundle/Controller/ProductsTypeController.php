<?php

namespace ProductsBundle\Controller;


use ProductsBundle\Entity\ProductType;
use ProductsBundle\Form\ProductsTypeForm\ProductTypeType;
use ProductsBundle\Repository\ProductTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProductsTypeController extends Controller
{

    public function indexAction()
    {

        /**
         * @var $productTypeRep ProductTypeRepository
         */
        $productTypeRep = $this
            ->getDoctrine()
            ->getRepository(ProductType::class);

        $productTypes = $productTypeRep->getListProductsTypeArray();

        return $this->render('@products/ProductTypes/index.html.twig', [
            'productTypes' => $productTypes,
        ]);
    }

    public function addAction(Request $request)
    {

        $productType = new ProductType();
        $form = $this->createForm(ProductTypeType::class, $productType);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()
                ->getManager();

            $em->persist($productType);
            $em->flush();

            return $this->redirectToRoute('products_type_index');
        }

        return $this->render('@products/ProductTypes/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function deleteAction($productTypeID)
    {
        $em = $this->getDoctrine()
            ->getManager();

        $typeRep = $em->getRepository(ProductType::class);

        $typeProduct = $typeRep->find($productTypeID);

        $em->remove($typeProduct);
        $em->flush();
        return $this->redirectToRoute('products_type_index');
    }

    public function modifyAction($productTypeID, Request $request)
    {
        $em = $this->getDoctrine()
            ->getManager();

        $productTypeRep = $em->getRepository(ProductType::class);

        $productType = $productTypeRep->find($productTypeID);
        $form = $this->createForm(ProductTypeType::class, $productType);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->flush();

            return $this->redirectToRoute('products_type_index');
        }

        return $this->render('@products/ProductTypes/modify.html.twig', [
            'form' => $form->createView(),
        ]);
    }

}

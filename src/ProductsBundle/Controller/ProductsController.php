<?php

namespace ProductsBundle\Controller;


use ProductsBundle\Entity\Product;
use ProductsBundle\Entity\ProductType;
use ProductsBundle\Repository\ProductRepository;
use ProductsBundle\Repository\ProductTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ProductsBundle\Form;
use Symfony\Component\HttpFoundation\Request;

class ProductsController extends Controller
{

    public function indexAction()
    {
        /**
         * @var $productTypeRepository ProductTypeRepository
         */
        $productTypeRepository = $this
            ->getDoctrine()
            ->getRepository(ProductType::class);

        $productTypes = $productTypeRepository->getListProductsTypeWithProductsArray();

        return $this->render('@products/Products/index.html.twig', [
            'productTypes' => $productTypes,
        ]);
    }

    public function allAction()
    {
        $em = $this->getDoctrine()
            ->getManager();

        /**
         * @var $productRep ProductRepository
         */
        $productRep = $em->getRepository(Product::class);

        $products = $productRep->getListProductsArray();

        return $this->render('@products/Products/all.html.twig', [
            'products' => $products,
        ]);
    }

    public function addAction(Request $request)
    {
        $product = new Product();

        $form = $this->createForm(Form\ProductsForm\ProductType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()
                ->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('products_products_index');
        }

        return $this->render('@products/Products/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function modifyAction($productTypeID, Request $request)
    {
        $em = $this->getDoctrine()
            ->getManager();
        $productRep = $em->getRepository(Product::class);
        $product = $productRep->find($productTypeID);

        $form = $this->createForm(Form\ProductsForm\ProductType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->redirectToRoute('products_products_index');
        }

        return $this->render('@products/Products/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function deleteAction($productTypeID)
    {
        $em = $this->getDoctrine()
            ->getManager();

        $productRep = $em->getRepository(Product::class);

        $product = $productRep->find($productTypeID);

        $em->remove($product);
        $em->flush();
        return $this->redirectToRoute('products_products_index');
    }

}

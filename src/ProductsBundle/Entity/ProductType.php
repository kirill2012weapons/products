<?php

namespace ProductsBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints  as Assert;

/**
 * Class ProductType
 * @package ProductsBundle\Entity
 * @ORM\Entity(repositoryClass="ProductsBundle\Repository\ProductTypeRepository")
 * @ORM\Table(name="tbl_product_types")
 * @ORM\HasLifecycleCallbacks()
 */
class ProductType
{

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(
     *     type="integer"
     * )
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(
     *     type="string",
     *     name="name",
     *     length=50,
     *     nullable=false
     * )
     * @Assert\NotBlank(
     *     message="Name Must Not be Blank!"
     * )
     * @Assert\Regex(
     *     pattern="/^[A-Za-zА-ЯЁа-яё][0-9A-Za-zА-ЯЁа-яё\_\-\s]+[A-Za-zА-ЯЁа-яё]$/",
     *     message="{{ value }} - not write! Must be start with characters Can contain spaces and: '-_[0-9][A-Za-zА-ЯЁа-яё]'"
     * )
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(
     *     type="datetime",
     *     name="created_at",
     *     nullable=false,
     *     length=19
     * )
     */
    private $createdAt = null;

    /**
     * @var string
     * @ORM\Column(
     *     type="datetime",
     *     name="modified_at",
     *     nullable=true,
     *     length=19
     * )
     */
    private $modifiedAt = null;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Product",
     *     mappedBy="productType",
     *     cascade={"remove"}
     * )
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function __toString() {

        return $this->getName();

    }

    /**
     * @return ArrayCollection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @return void
     * @param Product $product
     */
    public function setProduct(Product $product)
    {
        $this->products->add($product);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param string $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * @return void
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateTimeStamp()
    {
        try{
            $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));
            if($this->getCreatedAt() == null) $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        } catch (\Exception $exception) {
            echo $exception->getMessage();
            exit(0);
        }
    }

}
<?php

namespace ProductsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Product
 * @package ProductsBundle\Entity
 * @ORM\Entity(repositoryClass="ProductsBundle\Repository\ProductRepository")
 * @ORM\Table(name="tbl_products")
 * @ORM\HasLifecycleCallbacks()
 */
class Product
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(
     *     type="integer"
     * )
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(
     *     type="string",
     *     name="name",
     *     length=150,
     *     nullable=false
     * )
     * @Assert\NotBlank(
     *     message="Name Must Not be Blank!"
     * )
     * @Assert\Regex(
     *     pattern="/^[A-Za-zА-ЯЁа-яё][0-9A-Za-zА-ЯЁа-яё\_\-\s]+[A-Za-zА-ЯЁа-яё]$/",
     *     message="{{ value }} - not write! Must be start with characters Can contain spaces and: '-_[0-9][A-Za-zА-ЯЁа-яё]'"
     * )
     */
    private $name;

    /**
     * @var integer
     * @ORM\Column(
     *     type="decimal",
     *     precision=10,
     *     scale=2,
     *     name="price",
     *     nullable=false
     * )
     * @Assert\NotBlank(
     *     message="Price Must not be blank!"
     * )
     * @Assert\Regex(
     *     pattern="/^[0-9]{0,8}[\\,\\.]?[0-9]{0,2}$/",
     *     message="{{ value }} - Not correct, Must contains only [0-9] with '.' or ',' example - 99.9 or 143,34"
     * )
     */
    private $price;

    /**
     * @var string
     * @ORM\Column(
     *     type="datetime",
     *     name="created_at",
     *     nullable=false,
     *     length=19
     * )
     */
    private $createdAt = null;

    /**
     * @var string
     * @ORM\Column(
     *     type="datetime",
     *     name="modified_at",
     *     nullable=true,
     *     length=19
     * )
     */
    private $modifiedAt = null;

    /**
     * @var ProductType
     * @ORM\ManyToOne(
     *     targetEntity="ProductType",
     *     inversedBy="products"
     * )
     * @ORM\JoinColumn(
     *     name="product_type_id",
     *     referencedColumnName="id"
     * )
     */
    private $productType;

    /**
     * @return void
     * @param ProductType $productType
     */
    public function setProductType(ProductType $productType)
    {
        $productType->setProduct($this);
        $this->productType = $productType;
    }

    /**
     * @return ProductType
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param string $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * @return void
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateTimeStamp()
    {
        try{
            $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));
            if($this->getCreatedAt() == null) $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        } catch (\Exception $exception) {
            echo $exception->getMessage();
            exit(0);
        }
    }
}